use runestick::{Any, ContextError, Module};
use thiserror::Error;

/// An error occurred while waiting for next event
#[derive(Clone, Copy, Debug, Eq, PartialEq, Error, Any)]
pub enum ReceiveEventError {
    #[error("event channel closed (no senders)")]
    Closed,
    #[error("event stream lagged by {0} events")]
    Lagged(u64),
}

impl ReceiveEventError {
    /// Register rune type
    pub fn rune_register(module: &mut Module) -> Result<(), ContextError> {
        module.ty::<Self>()?;
        module.inst_fn("is_closed", |s: &Self| *s == Self::Closed)?;
        module.inst_fn("is_lagged", |s: &Self| match s {
            Self::Lagged(_) => true,
            _ => false,
        })?;
        module.inst_fn("lag_amount", |s: &Self| match s {
            Self::Lagged(v) => Some(*v),
            _ => None,
        })?;
        Ok(())
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq, Error, Any)]
pub enum SendEventError {
    #[error("event channel closed (no receivers)")]
    Closed,
}
