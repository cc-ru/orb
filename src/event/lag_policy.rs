/// How should event stream lag be handled?
pub trait LagPolicy: Clone {
    fn should_error(&self) -> bool;
}

/// Return error on event stream lag
#[derive(Clone)]
pub struct ErrorOnLag;

impl LagPolicy for ErrorOnLag {
    fn should_error(&self) -> bool {
        true
    }
}

/// Proceed normally on lag
#[derive(Clone)]
pub struct ProceedOnLag;

impl LagPolicy for ProceedOnLag {
    fn should_error(&self) -> bool {
        false
    }
}

/// Allows changing lag policy at runtime
///
/// The underlying boolean tells whether or not errors should be returned
/// when the stream lags behind
#[derive(Clone)]
pub struct DynamicLagPolicy(pub bool);

impl LagPolicy for DynamicLagPolicy {
    fn should_error(&self) -> bool {
        self.0
    }
}
