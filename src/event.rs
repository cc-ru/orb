pub mod error;
pub mod lag_policy;

use parking_lot::RwLock;
use std::any::{Any as StdAny, TypeId};
use std::collections::HashMap;
use std::sync::Arc;

use runestick::{Any, ContextError, Module, ToValue, Value};
use tokio::sync::broadcast::error::RecvError;
use tokio::sync::broadcast::{self, Receiver, Sender};

use self::error::{ReceiveEventError, SendEventError};
use self::lag_policy::{DynamicLagPolicy, ErrorOnLag, LagPolicy, ProceedOnLag};

type BoxAny = Box<dyn Send + Sync + StdAny>;
type RuneEventStreamBuilder = Box<dyn Send + Sync + Fn(&BoxAny) -> RuneEventStream>;

#[derive(Default)]
struct EventBusInner {
    senders: HashMap<TypeId, BoxAny>,
    rune_events: HashMap<&'static str, (RuneEventStreamBuilder, TypeId)>,
}

/// Event bus
#[derive(Clone, Default, Any)]
pub struct EventBus {
    inner: Arc<RwLock<EventBusInner>>,
}

impl EventBus {
    /// Create a new event bus
    pub fn new() -> EventBus {
        EventBus::default()
    }

    // TODO: investigate if it will be actually better to wrap all events in Arc
    /// Register an event
    pub fn register<T: Event>(&mut self, capacity: usize) {
        let mut inner = self.inner.write();
        let (sender, _receiver) = broadcast::channel::<T>(capacity);
        inner.senders.insert(TypeId::of::<T>(), Box::new(sender));
    }

    /// Register an event to be used in Rune. `register` should be called beforehand
    pub fn register_rune<T: RuneEvent>(&mut self, name: &'static str) {
        let id = TypeId::of::<T>();
        let stream_builder = Box::new(|any: &BoxAny| {
            let receiver = any
                .downcast_ref::<Sender<T>>()
                .expect("cannot downcast event sender")
                .subscribe();
            let stream = EventStream {
                lag_policy: DynamicLagPolicy(false),
                receiver,
            };
            RuneEventStream(Box::new(stream))
        });

        let mut inner = self.inner.write();
        inner.rune_events.insert(name, (stream_builder, id));
    }

    /// Subscribe to the event
    pub fn subscribe<T: Event>(&self) -> EventStream<T> {
        let inner = self.inner.read();

        let receiver = inner
            .senders
            .get(&TypeId::of::<T>())
            .expect("event is not registered")
            .downcast_ref::<Sender<T>>()
            .expect("cannot downcast event sender")
            .subscribe();
        EventStream {
            lag_policy: ProceedOnLag,
            receiver,
        }
    }

    /// Get event sender
    pub fn get_sender<T: Event>(&self) -> EventSender<T> {
        let inner = self.inner.read();
        let sender = inner
            .senders
            .get(&TypeId::of::<T>())
            .expect("event is not registered")
            .downcast_ref::<Sender<T>>()
            .expect("cannot downcast event sender")
            .clone();
        EventSender { sender }
    }

    pub fn subscribe_rune(&self, name: &str) -> RuneEventStream {
        let inner = self.inner.read();

        let (builder, id) = inner
            .rune_events
            .get(name)
            .expect("rune event is not registered");
        let sender = inner.senders.get(id).expect("event is not registered");
        builder(sender)
    }

    /// Register rune type
    pub fn rune_register(module: &mut Module) -> Result<(), ContextError> {
        module.ty::<Self>()?;
        module.inst_fn("subscribe", Self::subscribe_rune)?;
        Ok(())
    }
}

/// Event stream
pub struct EventStream<T: Event, LP: LagPolicy = ProceedOnLag> {
    pub lag_policy: LP,
    receiver: Receiver<T>,
}

impl<T: Event, LP: LagPolicy> EventStream<T, LP> {
    /// Get next event in the stream
    pub async fn next(&mut self) -> Result<T, ReceiveEventError> {
        loop {
            return match self.receiver.recv().await {
                Ok(v) => Ok(v),
                Err(RecvError::Lagged(v)) if self.lag_policy.should_error() => {
                    Err(ReceiveEventError::Lagged(v))
                }
                Err(RecvError::Closed) => Err(ReceiveEventError::Closed),
                Err(_) => {
                    // try again
                    continue;
                }
            };
        }
    }

    /// Proceed normally on lag
    pub fn proceed_on_lag(self) -> EventStream<T, ProceedOnLag> {
        EventStream {
            lag_policy: ProceedOnLag,
            receiver: self.receiver,
        }
    }

    /// Error on lag
    pub fn error_on_lag(self) -> EventStream<T, ErrorOnLag> {
        EventStream {
            lag_policy: ErrorOnLag,
            receiver: self.receiver,
        }
    }

    /// Allow changing lag policy arbitrarily
    pub fn dynamic_lag_policy(self) -> EventStream<T, DynamicLagPolicy> {
        EventStream {
            lag_policy: DynamicLagPolicy(self.lag_policy.should_error()),
            receiver: self.receiver,
        }
    }
}

#[async_trait::async_trait]
trait DynRuneEventStream {
    async fn next(&mut self) -> Result<Value, ReceiveEventError>;
    fn set_error_on_lag(&mut self, v: bool);
    fn get_error_on_lag(&self) -> bool;
}

#[async_trait::async_trait]
impl<T: RuneEvent> DynRuneEventStream for EventStream<T, DynamicLagPolicy> {
    async fn next(&mut self) -> Result<Value, ReceiveEventError> {
        self.next()
            .await
            .map(|v| v.to_value().expect("cannot convert"))
    }

    fn set_error_on_lag(&mut self, v: bool) {
        self.lag_policy.0 = v;
    }

    fn get_error_on_lag(&self) -> bool {
        self.lag_policy.0
    }
}

#[derive(Any)]
#[rune(name = "EventStream")]
pub struct RuneEventStream(Box<dyn DynRuneEventStream>);

impl RuneEventStream {
    pub async fn next(&mut self) -> Result<Value, ReceiveEventError> {
        self.0.next().await
    }

    pub fn set_error_on_lag(&mut self, v: bool) {
        self.0.set_error_on_lag(v);
    }

    pub fn get_error_on_lag(&self) -> bool {
        self.0.get_error_on_lag()
    }

    /// Register rune type
    pub fn rune_register(module: &mut Module) -> Result<(), ContextError> {
        module.ty::<Self>()?;
        module.async_inst_fn("next", Self::next)?;
        module.inst_fn("set_error_on_lag", Self::set_error_on_lag)?;
        module.inst_fn("get_error_on_lag", Self::get_error_on_lag)?;
        Ok(())
    }
}

/// Event sender
#[derive(Clone)]
pub struct EventSender<T: Event> {
    sender: Sender<T>,
}

impl<T: Event> EventSender<T> {
    /// Send an event
    pub fn send(&self, event: T) -> Result<(), SendEventError> {
        match self.sender.send(event) {
            Ok(_) => Ok(()),
            Err(_) => Err(SendEventError::Closed),
        }
    }
}

/// Marker trait for all valid events
pub trait Event: Clone + Send + Sync + 'static {}

impl<T: Clone + Send + Sync + 'static> Event for T {}

/// Marker trait for all events accessible from Rune
pub trait RuneEvent: Event + ToValue {}

impl<T: Event + ToValue> RuneEvent for T {}
