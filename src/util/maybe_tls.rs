use std::pin::Pin;
use std::sync::Arc;
use std::task::{Context, Poll};

use anyhow::{anyhow, Context as _, Result};
use once_cell::sync::Lazy;
use tokio::io::{AsyncRead, AsyncWrite, Error, ReadBuf};
use tokio::net::TcpStream;
use tokio::pin;
use tokio_rustls::client::TlsStream;
use tokio_rustls::rustls::ClientConfig;
use tokio_rustls::webpki::DNSNameRef;
use tokio_rustls::TlsConnector;
use webpki_roots::TLS_SERVER_ROOTS;

pub static TLS_CONFIG: Lazy<Arc<ClientConfig>> = Lazy::new(|| {
    let mut config = ClientConfig::new();
    config
        .root_store
        .add_server_trust_anchors(&TLS_SERVER_ROOTS);
    Arc::new(config)
});

#[derive(Debug)]
pub enum MaybeTlsStream {
    Raw(TcpStream),
    Tls(TlsStream<TcpStream>),
}

impl MaybeTlsStream {
    /// Tries to connect with TLS falling back to raw TCP in case of error
    pub async fn connect_auto(domain: &str, port: u16) -> Result<MaybeTlsStream> {
        let raw_stream = TcpStream::connect((domain, port))
            .await
            .with_context(|| anyhow!("Cannot connect to {}:{}", domain, port))?;
        let dns_name = match DNSNameRef::try_from_ascii_str(domain) {
            Ok(v) => v,
            Err(_) => return Ok(MaybeTlsStream::Raw(raw_stream)),
        };
        let connector = TlsConnector::from(TLS_CONFIG.clone());
        match connector
            .connect(dns_name, raw_stream)
            .into_failable()
            .await
        {
            Ok(v) => Ok(MaybeTlsStream::Tls(v)),
            Err((_, v)) => Ok(MaybeTlsStream::Raw(v)),
        }
    }
}

impl AsyncRead for MaybeTlsStream {
    fn poll_read(
        self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &mut ReadBuf<'_>,
    ) -> Poll<Result<(), Error>> {
        match self.get_mut() {
            MaybeTlsStream::Raw(stream) => {
                pin!(stream);
                stream.poll_read(cx, buf)
            }
            MaybeTlsStream::Tls(stream) => {
                pin!(stream);
                stream.poll_read(cx, buf)
            }
        }
    }
}

impl AsyncWrite for MaybeTlsStream {
    fn poll_write(
        self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &[u8],
    ) -> Poll<Result<usize, Error>> {
        match self.get_mut() {
            MaybeTlsStream::Raw(stream) => {
                pin!(stream);
                stream.poll_write(cx, buf)
            }
            MaybeTlsStream::Tls(stream) => {
                pin!(stream);
                stream.poll_write(cx, buf)
            }
        }
    }

    fn poll_flush(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Result<(), Error>> {
        match self.get_mut() {
            MaybeTlsStream::Raw(stream) => {
                pin!(stream);
                stream.poll_flush(cx)
            }
            MaybeTlsStream::Tls(stream) => {
                pin!(stream);
                stream.poll_flush(cx)
            }
        }
    }

    fn poll_shutdown(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Result<(), Error>> {
        match self.get_mut() {
            MaybeTlsStream::Raw(stream) => {
                pin!(stream);
                stream.poll_shutdown(cx)
            }
            MaybeTlsStream::Tls(stream) => {
                pin!(stream);
                stream.poll_shutdown(cx)
            }
        }
    }
}
