use super::protocol::Message;

pub trait MessageFilter: Send + Sync + 'static {
    fn filter(&self, msg: &Message) -> bool;
}

impl MessageFilter for u16 {
    fn filter(&self, msg: &Message) -> bool {
        msg.code() == Some(*self)
    }
}

impl MessageFilter for &'static [u16] {
    fn filter(&self, msg: &Message) -> bool {
        let code = match msg.code() {
            Some(v) => v,
            None => return false,
        };
        self.contains(&code)
    }
}

impl MessageFilter for &'static str {
    fn filter(&self, msg: &Message) -> bool {
        &msg.command == self
    }
}

impl MessageFilter for &'static [&'static str] {
    fn filter(&self, msg: &Message) -> bool {
        self.contains(&msg.command.as_str())
    }
}

impl<F: 'static + Send + Sync + Fn(&Message) -> bool> MessageFilter for F {
    fn filter(&self, msg: &Message) -> bool {
        self(msg)
    }
}
