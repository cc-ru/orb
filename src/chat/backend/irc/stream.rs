use std::fmt::Write as _;
use std::marker::Unpin;
use std::str::FromStr;

use anyhow::{Context, Result};
use tokio::io::{AsyncRead, AsyncReadExt, AsyncWrite, AsyncWriteExt};

use super::protocol::Message;
use crate::util::MaybeTlsStream;

pub struct IrcStream<IO = MaybeTlsStream> {
    stream: IO,
    buffer: Box<[u8]>,
    buffer_len: usize,
    skip_long: bool,
    write_buffer: String,
}

impl<IO: Unpin> IrcStream<IO> {
    pub fn new(stream: IO, buf_size: usize) -> IrcStream<IO> {
        assert!(buf_size >= 3);

        IrcStream {
            stream,
            buffer: vec![0; buf_size].into_boxed_slice(),
            buffer_len: 0,
            skip_long: false,
            write_buffer: String::new(),
        }
    }
}

impl<IO: AsyncRead + Unpin> IrcStream<IO> {
    pub async fn read(&mut self) -> Result<Message> {
        loop {
            while let Some(v) = self.take_message() {
                if let Ok(msg) = v {
                    return Ok(msg);
                }
            }

            let mut read_buf = &mut self.buffer[self.buffer_len..];
            if read_buf.is_empty() {
                self.skip_long = true;
                self.buffer_len = 1;
                self.buffer[0] = self.buffer[self.buffer.len() - 1];
                read_buf = &mut self.buffer[1..];
            }

            let num_read = self.stream.read(read_buf).await.context("Read error")?;
            self.buffer_len += num_read;
        }
    }

    fn take_message(&mut self) -> Option<Result<Message, ()>> {
        let newline_pos = self.buffer[..self.buffer_len]
            .windows(2)
            .position(|s| s == b"\r\n")?;
        let ret = if self.skip_long {
            self.skip_long = false;
            tracing::error!("Message too long");
            Some(Err(()))
        } else {
            Some(Self::parse_message(&self.buffer[..newline_pos]))
        };

        self.buffer.copy_within(newline_pos + 2..self.buffer_len, 0);
        self.buffer_len -= newline_pos + 2;

        ret
    }

    fn parse_message(line: &[u8]) -> Result<Message, ()> {
        let str = String::from_utf8_lossy(line);
        let res = Message::from_str(str.as_ref());
        if res.is_err() {
            tracing::error!(message = str.as_ref(), "Bad message")
        }

        res.map_err(|_| ())
    }
}

impl<IO: AsyncWrite + Unpin> IrcStream<IO> {
    pub async fn write(&mut self, msg: Message) -> Result<()> {
        write!(&mut self.write_buffer, "{}\r\n", msg)?;
        self.stream
            .write_all(self.write_buffer.as_bytes())
            .await
            .context("Write error")?;
        self.write_buffer.clear();
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::io::Cursor;

    #[tokio::test]
    async fn simple() {
        let cursor = Cursor::new(b"HELLO\r\nWORLD\r\n");
        let mut stream = IrcStream::new(cursor, 7);
        assert_eq!(stream.read().await.unwrap(), Message::new("HELLO"));
        assert_eq!(stream.read().await.unwrap(), Message::new("WORLD"));
    }

    #[tokio::test]
    async fn big_buffer() {
        let cursor = Cursor::new(b"HELLO\r\nWORLD\r\n");
        let mut stream = IrcStream::new(cursor, 100);
        assert_eq!(stream.read().await.unwrap(), Message::new("HELLO"));
        assert_eq!(stream.read().await.unwrap(), Message::new("WORLD"));
    }

    #[tokio::test]
    async fn long_message() {
        let cursor = Cursor::new(b"HELLO\r\nA\r\n");
        let mut stream = IrcStream::new(cursor, 3);
        assert_eq!(stream.read().await.unwrap(), Message::new("A"));

        let cursor = Cursor::new(b"HELLO\r\nA\r\n");
        let mut stream = IrcStream::new(cursor, 7);
        assert_eq!(stream.read().await.unwrap(), Message::new("HELLO"));
        assert_eq!(stream.read().await.unwrap(), Message::new("A"));
    }

    #[tokio::test]
    async fn bad_message() {
        let cursor = Cursor::new(b"@:A\r\nA\r\n");
        let mut stream = IrcStream::new(cursor, 16);
        assert_eq!(stream.read().await.unwrap(), Message::new("A"));
    }
}
