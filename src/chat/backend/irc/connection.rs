use std::collections::HashMap;
use std::future::Future;
use std::time::Duration;

use anyhow::{anyhow, bail, Result};
use slab::Slab;

use super::protocol::Message;
use super::stream::IrcStream;
use crate::chat::backend::ChannelId;

pub struct ChannelEntry {
    name: String,
    joined: bool,
}

struct Channels(Slab<ChannelEntry>);

impl Channels {
    fn insert(&mut self, channel: ChannelEntry) -> ChannelId {
        ChannelId(self.0.insert(channel) as _)
    }

    fn get(&self, id: ChannelId) -> Option<&ChannelEntry> {
        self.0.get(id.0 as _)
    }

    fn get_mut(&mut self, id: ChannelId) -> Option<&mut ChannelEntry> {
        self.0.get_mut(id.0 as _)
    }
}

impl std::ops::Index<ChannelId> for Channels {
    type Output = ChannelEntry;
    fn index(&self, id: ChannelId) -> &ChannelEntry {
        self.get(id).unwrap()
    }
}

impl std::ops::IndexMut<ChannelId> for Channels {
    fn index_mut(&mut self, id: ChannelId) -> &mut ChannelEntry {
        self.get_mut(id).unwrap()
    }
}

async fn wrap_timeout<T>(timeout: Duration, fut: impl Future<Output = Result<T>>) -> Result<T> {
    tokio::time::timeout(timeout, fut)
        .await
        .map_err(|_| anyhow!("Timed out"))?
}

pub struct Connection {
    stream: IrcStream,
    username: String,
    channels: Channels,
    name_to_id: HashMap<String, ChannelId>,
}

impl Connection {
    async fn send(&mut self, msg: Message) -> Result<()> {
        self.stream.write(msg).await
    }

    async fn recv(&mut self) -> Result<Message> {
        let msg = self.stream.read().await?;
        self.handle_message(&msg).await?;
        Ok(msg)
    }

    async fn recv_filter(&mut self, filter: impl Fn(&Message) -> bool) -> Result<Message> {
        loop {
            let msg = self.recv().await?;
            if filter(&msg) {
                return Ok(msg);
            }
        }
    }

    async fn recv_cmd_or_code(&mut self, cmd: &str, codes: &[u16]) -> Result<Message> {
        self.recv_filter(|msg| match msg.code() {
            Some(v) if codes.contains(&v) => true,
            None if msg.command == cmd => true,
            _ => false,
        })
        .await
    }

    async fn handle_message(&mut self, msg: &Message) -> Result<()> {
        if msg.command == "PING" {
            let mut res = Message::new("PONG");
            res.params = msg.params.clone();
            self.send(res).await?;
        }

        Ok(())
    }

    fn new_channel(&mut self, name: String) -> Result<ChannelId> {
        if let Some(id) = self.name_to_id.get(&name) {
            return Ok(*id);
        }

        if name.is_empty() || name.contains(' ') {
            bail!("Bad channel name")
        }

        let channel = ChannelEntry {
            name,
            joined: false,
        };

        Ok(self.channels.insert(channel))
    }

    fn get_channel(&mut self, id: ChannelId) -> Result<&mut ChannelEntry> {
        match self.channels.get_mut(id) {
            Some(v) => Ok(v),
            None => bail!("No such channel"),
        }
    }

    async fn join(&mut self, id: ChannelId) -> Result<()> {
        let channel = self.get_channel(id)?;
        if channel.joined {
            bail!("Already joined");
        }

        let name = channel.name.clone();
        self.send(Message::new("JOIN").with_param(&name)).await?;

        let codes = &[403, 405, 475, 474, 471, 473];
        let recv = self.recv_cmd_or_code("JOIN", codes);
        let res = wrap_timeout(Duration::from_secs(4), recv).await?;
        if res.command == "JOIN" {
            self.channels[id].joined = true;
        } else {
            bail!("{} (code {})", res.error_reason(), res.command);
        }

        Ok(())
    }

    async fn part(&mut self, id: ChannelId) -> Result<()> {
        let channel = self.get_channel(id)?;
        if !channel.joined {
            bail!("Not joined");
        }

        let name = channel.name.clone();
        self.send(Message::new("PART").with_param(&name)).await?;

        let codes = &[403, 442];
        let recv = self.recv_cmd_or_code("PART", codes);
        let res = wrap_timeout(Duration::from_secs(4), recv).await?;
        if res.command == "PART" {
            self.channels[id].joined = false;
        } else {
            bail!("{} (code {})", res.error_reason(), res.command);
        }

        Ok(())
    }

    async fn send_message(&mut self, id: ChannelId, text: String) -> Result<()> {
        let channel = self.get_channel(id)?;
        let name = channel.name.clone();
        self.send(Message::new("PRIVMSG").with_param(name).with_param(text))
            .await?;
        // TODO: error handle
        Ok(())
    }
}
