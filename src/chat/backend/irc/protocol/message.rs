use std::fmt::{self, Display};
use std::str::FromStr;

use super::{ParseError, Prefix, Tag};

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Message {
    pub tags: Vec<Tag>,
    pub prefix: Option<Prefix>,
    pub command: String,
    pub params: Vec<String>,
}

impl Message {
    pub fn new(command: impl Into<String>) -> Self {
        Self {
            tags: Vec::new(),
            prefix: None,
            command: command.into(),
            params: Vec::new(),
        }
    }

    pub fn with_param(mut self, param: impl Into<String>) -> Self {
        self.params.push(param.into());
        self
    }

    pub fn with_prefix(mut self, prefix: Prefix) -> Self {
        self.prefix = Some(prefix);
        self
    }

    pub fn with_origin(mut self, origin: impl Into<String>) -> Self {
        if let Some(prefix) = &mut self.prefix {
            prefix.origin = origin.into();
        } else {
            self.prefix = Some(Prefix::new(origin.into()));
        }

        self
    }

    pub fn with_tag(mut self, tag: Tag) -> Self {
        self.tags.push(tag);
        self
    }

    pub fn response_target(&self) -> Option<&str> {
        if self.command != "PRIVMSG" {
            return None;
        }

        let target = self.params.get(0)?;

        if target.starts_with("#") {
            Some(&target)
        } else {
            Some(&self.prefix.as_ref()?.origin)
        }
    }

    pub fn code(&self) -> Option<u16> {
        self.command.parse().ok()
    }

    pub fn error_reason(&self) -> &str {
        self.params
            .last()
            .map(|v| v.as_str())
            .unwrap_or("No reason")
    }
}

impl Display for Message {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if !self.tags.is_empty() {
            f.write_str("@")?;
            self.tags[0].fmt(f)?;
            for tag in &self.tags[1..] {
                f.write_str(";")?;
                tag.fmt(f)?;
            }
            f.write_str(" ")?;
        }

        if let Some(prefix) = &self.prefix {
            f.write_str(":")?;
            prefix.fmt(f)?;
            f.write_str(" ")?;
        }

        f.write_str(&self.command)?;

        for param in self.params.iter().rev().skip(1).rev() {
            if param.contains(' ') {
                return Err(fmt::Error);
            }

            write!(f, " {}", param)?;
        }

        if let Some(param) = self.params.last() {
            if param.contains(' ') {
                write!(f, " :{}", param)?;
            } else {
                write!(f, " {}", param)?;
            }
        }

        Ok(())
    }
}

impl FromStr for Message {
    type Err = ParseError;

    fn from_str(mut line: &str) -> Result<Message, ParseError> {
        let mut tags = Vec::new();
        if line.starts_with('@') {
            let space = line.find(' ').ok_or(ParseError)?;
            tags = line[1..space]
                .split(';')
                .map(Tag::from_str)
                .collect::<Result<_, _>>()?;
            line = line[space..].trim_start();
        }

        let prefix = if line.starts_with(':') {
            let space = line.find(' ').ok_or(ParseError)?;
            let prefix = line[1..space].parse()?;
            line = line[space..].trim_start();
            Some(prefix)
        } else {
            None
        };

        let space = line.find(' ').unwrap_or_else(|| line.len());
        let command = line[..space].into();
        line = line[space..].trim_start();

        let mut params = vec![];

        while !line.is_empty() {
            if line.starts_with(':') {
                params.push(line[1..].into());
                break;
            } else {
                let space = line.find(' ').unwrap_or_else(|| line.len());
                params.push(line[..space].into());
                line = line[space..].trim_start();
            }
        }

        Ok(Message {
            tags,
            prefix,
            command,
            params,
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_and_format() {
        fn check(str: &str, msg: Message) {
            assert_eq!(&msg.to_string(), str);
            assert_eq!(str.parse(), Ok(msg));
        }

        check(
            "PRIVMSG #chan Hey!",
            Message::new("PRIVMSG")
                .with_param("#chan")
                .with_param("Hey!"),
        );

        check(
            ":dan!d@localhost PRIVMSG #chan :всем пока",
            Message::new("PRIVMSG")
                .with_param("#chan")
                .with_param("всем пока")
                .with_prefix(Prefix::new("dan").with_username("d").with_host("localhost")),
        );

        check(
            "@foo=bar;baz/qux=foo\\:bar;+ham :dan!d@localhost PRIVMSG #chan :всем пока",
            Message::new("PRIVMSG")
                .with_param("#chan")
                .with_param("всем пока")
                .with_prefix(Prefix::new("dan").with_username("d").with_host("localhost"))
                .with_tag(Tag::new("foo").with_value("bar"))
                .with_tag(Tag::new("qux").with_value("foo;bar").with_vendor("baz"))
                .with_tag(Tag::new("ham").with_client_only(true)),
        );
    }
}
