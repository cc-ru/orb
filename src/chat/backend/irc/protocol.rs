mod message;
mod prefix;
mod tag;

pub use self::message::Message;
pub use self::prefix::Prefix;
pub use self::tag::Tag;

use thiserror::Error;

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq, Error)]
#[error("IRC parse error")]
pub struct ParseError;
