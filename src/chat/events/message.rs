use runestick::{Any, ContextError, Module};

#[derive(Clone, Debug, Any)]
pub struct MessageEvent {
    author: String,
    text: String,
}

impl MessageEvent {
    /// Create a new message event
    pub fn new(author: String, text: String) -> MessageEvent {
        MessageEvent { author, text }
    }

    /// Get message author
    pub fn author(&self) -> &str {
        &self.author
    }

    /// Get message text
    // TODO: handle rich text
    pub fn text(&self) -> &str {
        &self.text
    }

    /// Register rune type
    pub fn rune_register(module: &mut Module) -> Result<(), ContextError> {
        module.ty::<Self>()?;
        module.inst_fn("author", |s: &MessageEvent| s.author.clone())?;
        module.inst_fn("text", |s: &MessageEvent| s.text.clone())?;
        Ok(())
    }
}
