use runestick::{Any, ContextError, Module};

#[derive(Clone, Debug, Any)]
pub struct PartEvent {
    subject: String,
}

impl PartEvent {
    /// Create a new part event
    pub fn new(subject: String) -> PartEvent {
        PartEvent { subject }
    }

    /// Get part subject
    pub fn subject(&self) -> &str {
        &self.subject
    }

    /// Register rune type
    pub fn rune_register(module: &mut Module) -> Result<(), ContextError> {
        module.ty::<Self>()?;
        module.inst_fn("subject", |s: &PartEvent| s.subject.clone())?;
        Ok(())
    }
}
