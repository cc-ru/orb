use runestick::{Any, ContextError, Module};

#[derive(Clone, Debug, Any)]
pub struct KickEvent {
    subject: String,
    object: String,
    reason: String,
}

impl KickEvent {
    /// Create a new action (/me) event
    pub fn new(subject: String, object: String, reason: String) -> KickEvent {
        KickEvent {
            subject,
            object,
            reason,
        }
    }

    /// Get kick subject (who performed the kick action)
    pub fn subject(&self) -> &str {
        &self.subject
    }

    /// Get kick object (who was kicked)
    pub fn object(&self) -> &str {
        &self.object
    }

    /// Get kick reason
    pub fn reason(&self) -> &str {
        &self.reason
    }

    /// Register rune type
    pub fn rune_register(module: &mut Module) -> Result<(), ContextError> {
        module.ty::<Self>()?;
        module.inst_fn("subject", |s: &KickEvent| s.subject.clone())?;
        module.inst_fn("object", |s: &KickEvent| s.object.clone())?;
        module.inst_fn("reason", |s: &KickEvent| s.reason.clone())?;
        Ok(())
    }
}
