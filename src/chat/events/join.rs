use runestick::{Any, ContextError, Module};

#[derive(Clone, Debug, Any)]
pub struct JoinEvent {
    subject: String,
}

impl JoinEvent {
    /// Create a new join event
    pub fn new(subject: String) -> JoinEvent {
        JoinEvent { subject }
    }

    /// Get join subject
    pub fn subject(&self) -> &str {
        &self.subject
    }

    /// Register rune type
    pub fn rune_register(module: &mut Module) -> Result<(), ContextError> {
        module.ty::<Self>()?;
        module.inst_fn("subject", |s: &JoinEvent| s.subject.clone())?;
        Ok(())
    }
}
