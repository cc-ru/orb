use runestick::{Any, ContextError, Module};

#[derive(Clone, Debug, Any)]
pub struct ActionEvent {
    author: String,
    text: String,
}

impl ActionEvent {
    /// Create a new action (/me) event
    pub fn new(author: String, text: String) -> ActionEvent {
        ActionEvent { author, text }
    }

    /// Get action author
    pub fn author(&self) -> &str {
        &self.author
    }

    /// Get action text
    // TODO: handle rich text
    pub fn text(&self) -> &str {
        &self.text
    }

    /// Register rune type
    pub fn rune_register(module: &mut Module) -> Result<(), ContextError> {
        module.ty::<Self>()?;
        module.inst_fn("author", |s: &ActionEvent| s.author.clone())?;
        module.inst_fn("text", |s: &ActionEvent| s.text.clone())?;
        Ok(())
    }
}
