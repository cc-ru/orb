use runestick::{Any, ContextError, Module};

#[derive(Clone, Debug, Any)]
pub struct NoticeEvent {
    author: String,
    text: String,
}

impl NoticeEvent {
    /// Create a new notice event
    pub fn new(author: String, text: String) -> NoticeEvent {
        NoticeEvent { author, text }
    }

    /// Get notice author
    pub fn author(&self) -> &str {
        &self.author
    }

    /// Get notice text
    // TODO: handle rich text
    pub fn text(&self) -> &str {
        &self.text
    }

    /// Register rune type
    pub fn rune_register(module: &mut Module) -> Result<(), ContextError> {
        module.ty::<Self>()?;
        module.inst_fn("author", |s: &NoticeEvent| s.author.clone())?;
        module.inst_fn("text", |s: &NoticeEvent| s.text.clone())?;
        Ok(())
    }
}
