use std::sync::Arc;

use anyhow::Result;
use runestick::{Any, ContextError, Module};

use super::backend::{ChannelId, ChatBackend};
use super::Connection;
use crate::event::EventBus;

/// Channel
#[derive(Any)]
pub struct Channel {
    backend: Arc<dyn ChatBackend>,
    id: ChannelId,
}

impl Channel {
    pub(super) fn new(backend: Arc<dyn ChatBackend>, id: ChannelId) -> Channel {
        Channel { backend, id }
    }

    /// Get channel name
    pub fn name(&self) -> String {
        self.backend.channel_name(self.id)
    }

    /// Are we currently joined to this channel?
    pub fn is_joined(&self) -> bool {
        self.backend.channel_is_joined(self.id)
    }

    /// Join to this channel
    pub async fn join(&self) -> Result<()> {
        if !self.is_joined() {
            return self.backend.channel_join(self.id).await;
        }
        Ok(())
    }

    /// Part from this channel
    pub async fn part(&self) -> Result<()> {
        if self.is_joined() {
            return self.backend.channel_part(self.id).await;
        }
        Ok(())
    }

    /// Send a regular message to the channel
    pub async fn send(&self, message: String) -> Result<()> {
        self.backend.channel_send(self.id, message).await
    }

    /// Send a notice message to the channel
    pub async fn send_notice(&self, message: String) -> Result<()> {
        self.backend.channel_send_notice(self.id, message).await
    }

    /// Send an action message to the channel
    pub async fn send_action(&self, message: String) -> Result<()> {
        self.backend.channel_send_action(self.id, message).await
    }

    /// Get the list of channel users
    pub async fn list_users(&self) -> Result<Vec<String>> {
        self.backend.channel_list_users(self.id).await
    }

    /// Get event bus of this channel
    pub fn get_event_bus(&self) -> EventBus {
        self.backend.channel_get_event_bus(self.id)
    }

    /// Get underlying connection
    pub fn get_connection(&self) -> Connection {
        Connection::new(self.backend.clone())
    }

    /// Register rune type
    pub fn rune_register(module: &mut Module) -> Result<(), ContextError> {
        module.ty::<Self>()?;
        module.inst_fn("name", Self::name)?;
        module.async_inst_fn("send", Self::send)?;
        module.async_inst_fn("send_notice", Self::send_notice)?;
        module.async_inst_fn("send_action", Self::send_action)?;
        module.async_inst_fn("list_users", Self::list_users)?;
        module.inst_fn("get_event_bus", Self::get_event_bus)?;
        module.inst_fn("get_connection", Self::get_connection)?;
        Ok(())
    }
}
