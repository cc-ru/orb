pub mod irc;

use anyhow::Result;
use async_trait::async_trait;

use crate::event::EventBus;

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct ChannelId(pub u32);

#[async_trait]
pub trait ChatBackend: Send + Sync + 'static {
    fn get_channel(&self, name: &str) -> Result<ChannelId>;

    fn list_joined_channels(&self) -> Result<Vec<ChannelId>>;

    async fn is_user_identified(&self, user: &str) -> Result<bool>;

    fn channel_name(&self, id: ChannelId) -> String;

    fn channel_is_joined(&self, id: ChannelId) -> bool;

    async fn channel_join(&self, id: ChannelId) -> Result<()>;

    async fn channel_part(&self, id: ChannelId) -> Result<()>;

    async fn channel_send(&self, id: ChannelId, message: String) -> Result<()>;

    async fn channel_send_notice(&self, id: ChannelId, message: String) -> Result<()>;

    async fn channel_send_action(&self, id: ChannelId, message: String) -> Result<()>;

    async fn channel_list_users(&self, id: ChannelId) -> Result<Vec<String>>;

    fn channel_get_event_bus(&self, id: ChannelId) -> EventBus;
}
