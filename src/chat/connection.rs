use std::sync::Arc;

use anyhow::Result;
use runestick::{Any, ContextError, Module};

use super::backend::ChatBackend;
use super::Channel;

/// Chat connection
#[derive(Any)]
pub struct Connection {
    backend: Arc<dyn ChatBackend>,
}

impl Connection {
    pub(super) fn new(backend: Arc<dyn ChatBackend>) -> Connection {
        Connection { backend }
    }

    /// Join a channel. `name` can be a username, or a group channel name
    pub async fn join_channel(&self, name: &str) -> Result<Channel> {
        let id = self.backend.get_channel(name)?;
        let channel = Channel::new(self.backend.clone(), id);
        channel.join().await?;
        Ok(channel)
    }

    /// Get list of currently joined channels
    pub fn list_joined_channels(&self) -> Vec<Channel> {
        todo!()
    }

    /// Check if the specified user is identified
    pub async fn is_user_identified(&self, username: &str) -> Result<bool> {
        self.backend.is_user_identified(username).await
    }

    /// Register rune type
    pub fn rune_register(module: &mut Module) -> Result<(), ContextError> {
        module.ty::<Self>()?;
        module.async_inst_fn("join_channel", Self::join_channel)?;
        module.inst_fn("list_joined_channels", Self::list_joined_channels)?;
        module.async_inst_fn("is_user_identified", Self::is_user_identified)?;
        Ok(())
    }
}
